# YOLOv8 多路视频流推理工具

## 简介
本仓库提供了一个资源文件，用于实现 YOLOv8 模型同时对多路视频流进行推理，并且支持使用 PyTorch 和 ONNX 两种推理引擎。通过本工具，您可以轻松地在多个视频流上并行运行 YOLOv8 模型，从而提高推理效率和处理速度。

## 功能特点
- **多路视频流推理**：支持同时对多个视频流进行推理，适用于需要处理大量视频数据的场景。
- **支持 PyTorch 和 ONNX**：提供两种推理引擎选择，满足不同用户的需求。
- **高效并行处理**：通过并行处理技术，显著提升推理速度，减少等待时间。

## 使用方法
1. **克隆仓库**：
   ```bash
   git clone https://github.com/your-repo/yolov8-multi-stream-inference.git
   cd yolov8-multi-stream-inference
   ```

2. **安装依赖**：
   ```bash
   pip install -r requirements.txt
   ```

3. **配置文件**：
   根据您的需求，编辑配置文件 `config.yaml`，指定视频流路径、模型路径、推理引擎等信息。

4. **运行推理**：
   ```bash
   python main.py
   ```

## 示例
以下是一个简单的配置文件示例：
```yaml
video_streams:
  - path: "video1.mp4"
  - path: "video2.mp4"
model_path: "yolov8n.pt"
engine: "torch"  # 可选值：torch, onnx
```

## 贡献
欢迎大家贡献代码、提出问题或建议。如果您有任何改进的想法，请提交 Pull Request 或 Issue。

## 许可证
本项目采用 MIT 许可证，详情请参阅 [LICENSE](LICENSE) 文件。

---

希望本工具能够帮助您在多路视频流推理任务中取得更好的效果！